FROM php:7.1-fpm
MAINTAINER __ksh__

RUN apt-get update -y && apt-get install -y nginx supervisor less && \
rm -rf /var/lib/apt/lists/*
RUN mkdir -p /var/log/nginx /var/www/code
RUN sed -i -e "5 s/\[::\]:9000/\/tmp\/php-fpm.sock/g" /usr/local/etc/php-fpm.d/zz-docker.conf
RUN echo -n "listen.owner = www-data\nlisten.group = www-data" >> /usr/local/etc/php-fpm.d/zz-docker.conf

COPY config/nginx.conf /etc/nginx/sites-enabled/default
COPY config/nginx-supervisor.conf  /etc/supervisor/conf.d/
COPY config/phpfpm-supervisor.conf  /etc/supervisor/conf.d/